import React, { useContext } from 'react'
import { Context } from './App'

export default function Button () {
  const { dispatch } = useContext(Context)

  return (
    <>
      <button onClick={() => dispatch({type: 'INCREMENT'})}>+</button>
      <button onClick={() => dispatch({type: 'DECREMENT'})}>-</button>
    </>
  )
}