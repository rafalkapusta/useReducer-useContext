import React, { useReducer } from 'react'
import Page from './Page'

export const Context = React.createContext({})

const initialState = {
  counter: 0,
}

const reducer = (state, action) => {
  switch (action.type) {
    case 'INCREMENT':
      return {
        ...state,
        counter: state.counter + 1,
      }
    case 'DECREMENT':
      return {
        ...state,
        counter: state.counter - 1,
      }
    default:
      return state
  }
}

function App () {
  const [state, dispatch] = useReducer(reducer, initialState)

  return (
    <Context.Provider value={{state, dispatch}}>
      <Page/>
    </Context.Provider>
  )
}

export default App
