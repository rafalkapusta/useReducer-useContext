import React, { useContext } from 'react'
import {Context} from './App'

export default function Counter () {
  const {state} = useContext(Context)

  return (
    <div>
      <input type='text' value={state.counter}/>
    </div>
  )
}